from cpt.packager import ConanMultiPackager


if __name__ == "__main__":
    builder = ConanMultiPackager(build_policy="outdated", archs=["x86_64"],
                                 build_types=["Release"])
    builder.add()
    builder.run()
